// import 'dart:math';

// import 'package:flutter/material.dart';
// import 'package:think_math/const.dart';
// import 'package:think_math/util/my_button.dart';
// import 'package:think_math/util/result_message.dart';

// class HomePage extends StatefulWidget {
//   const HomePage({Key? key}) : super(key: key);

//   @override
//   State<HomePage> createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {
//   // number pad list
//   List<String> numberPad = [
//     '7',
//     '8',
//     '9',
//     'C',
//     '4',
//     '5',
//     '6',
//     'DEL',
//     '1',
//     '2',
//     '3',
//     '=',
//     '0'
//   ];

//   // number A, number B
//   int numberA = 1;
//   int numberB = 1;

//   // user answer
//   String userAnswer = '';

//   // user tapped a button
//   void buttonTapped(String button) {
//     setState(() {
//       if (button == '=') {
//         // calculate if user is correct or incorrect
//         checkResult();
//       } else if (button == 'C') {
//         // clear the input
//         userAnswer = '';
//       } else if (button == 'DEL') {
//         // delete the last number
//         if (userAnswer.isNotEmpty) {
//           userAnswer = userAnswer.substring(0, userAnswer.length - 1);
//         }
//       } else if (userAnswer.length < 3) {
//         // maximum of 3 number can be inputted
//         userAnswer += button;
//       }
//     });
//   }

//   // check if user is correct or not
//   void checkResult() {
//     if (numberA + numberB == int.parse(userAnswer)) {
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'Correct!',
//               onTap: goToNextQuestion,
//               icon: Icons.arrow_forward,
//             );
//           });
//     } else {
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'Sorry try again',
//               onTap: goBackToQuestion,
//               icon: Icons.rotate_left,
//             );
//           });
//     }
//   }

//   // create random numbers
//   var randomNumber = Random();

//   // go to next question
//   void goToNextQuestion() {
//     // dismiss alert dialog
//     Navigator.of(context).pop();

//     // reset values
//     setState(() {
//       userAnswer = '';
//     });

//     // create a new question
//     numberA = randomNumber.nextInt(10);
//     numberB = randomNumber.nextInt(10);
//   }

//   // go back to question
//   void goBackToQuestion() {
//     // dismiss alert dialog
//     Navigator.of(context).pop();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final bool isPortrait =
//         MediaQuery.of(context).orientation == Orientation.portrait;
//     final double headerHeight = isPortrait ? 160 : 60;
//     final double inputSize = isPortrait ? 160 : 60;
//     return Scaffold(
//       backgroundColor: Colors.deepPurple[300],
//       body: isPortrait
//           ? _buildPortraitLayout(headerHeight, inputSize)
//           : _buildLandscapeLayout(headerHeight, inputSize),
//     );
//   }

//   Widget _buildPortraitLayout(double headerHeight, double inputSize) {
//     return Column(
//       children: [
//         //level
//         Container(
//           height: headerHeight,
//           color: Colors.deepPurple,
//         ),

//         // question
//         Expanded(
//           child: Container(
//             child: Center(
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   // qeustion
//                   Text(
//                     numberA.toString() + ' + ' + numberB.toString() + ' = ',
//                     style: whiteTextStyle,
//                   ),

//                   // answer box
//                   Container(
//                     height: 50,
//                     width: 100,
//                     decoration: BoxDecoration(
//                       color: Colors.deepPurple[400],
//                       borderRadius: BorderRadius.circular(4),
//                     ),
//                     child: Center(
//                       child: Text(
//                         userAnswer,
//                         style: whiteTextStyle,
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),

//         // numpad
//         Expanded(
//           flex: 2,
//           child: Padding(
//             padding: const EdgeInsets.all(4.0),
//             child: GridView.builder(
//               itemCount: numberPad.length,
//               physics: const NeverScrollableScrollPhysics(),
//               gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//                 crossAxisCount: 4,
//               ),
//               itemBuilder: (context, index) {
//                 return MyButton(
//                   child: numberPad[index],
//                   onTap: () => buttonTapped(numberPad[index]),
//                 );
//               },
//             ),
//           ),
//         ),
//       ],
//     );
//   }

//   Widget _buildLandscapeLayout(double headerHeight, double inputSize) {
//     return Column(
//       children: [
//         Container(
//           height: headerHeight,
//           color: Colors.deepPurple,
//         ),
//         Expanded(
//           child: Row(
//             children: [
//               // question
//               Expanded(
//                 flex: 1,
//                 child: Container(
//                   height: double.infinity,
//                   color: Colors.deepPurple[400],
//                   padding: EdgeInsets.only(bottom: 70),
//                   child: Center(
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         // qeustion
//                         Text(
//                           numberA.toString() +
//                               ' + ' +
//                               numberB.toString() +
//                               ' = ',
//                           style: whiteTextStyle,
//                         ),

//                         // answer box
//                         Container(
//                           height: 50,
//                           width: 100,
//                           decoration: BoxDecoration(
//                             color: Colors.deepPurple,
//                             borderRadius: BorderRadius.circular(4),
//                           ),
//                           child: Center(
//                             child: Text(
//                               userAnswer,
//                               style: whiteTextStyle,
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),

//               // numpad
//               Expanded(
//                 flex: 1,
//                 child: Padding(
//                   padding: const EdgeInsets.all(4.0),
//                   child: GridView.builder(
//                     shrinkWrap: true,
//                     padding: EdgeInsets.zero,
//                     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                       crossAxisCount: 4,
//                       childAspectRatio: 1.5,
//                     ),
//                     itemCount: numberPad.length,
//                     itemBuilder: (context, index) {
//                       return MyButton(
//                         child: numberPad[index],
//                         onTap: () => buttonTapped(numberPad[index]),
//                       );
//                     },
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
// }
