import 'dart:async';
import 'dart:math';

import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter/material.dart';
import 'package:think_math/const.dart';
import 'package:think_math/menu.dart';
import 'package:think_math/util/my_button_sub.dart';
import 'package:think_math/util/result_message.dart';

import 'models/numberpadProvider.dart';

var randomNumber = Random();

class Sub extends StatefulWidget {
  const Sub({Key? key}) : super(key: key);

  @override
  State<Sub> createState() => _SubState();
}

class _SubState extends State<Sub> {
  // List<String> numberPad = [
  //   '7',
  //   '8',
  //   '9',
  //   'C',
  //   '4',
  //   '5',
  //   '6',
  //   'DEL',
  //   '1',
  //   '2',
  //   '3',
  //   '=',
  //   '0',
  //   '.',
  //   '-'
  // ];

  // number A, number B
  int numberA = randomNumber.nextInt(10);
  int numberB = randomNumber.nextInt(10);
  int score = 0;
  // user answer
  String userAnswer = '';

  // user tapped a button
  void buttonTapped(String button) {
    setState(() {
      if (button == '=') {
        // calculate if user is correct or incorrect
        checkResult();
      } else if (button == 'C') {
        // clear the input
        userAnswer = '';
      } else if (button == 'DEL') {
        // delete the last number
        if (userAnswer.isNotEmpty) {
          userAnswer = userAnswer.substring(0, userAnswer.length - 1);
        }
      } else if (userAnswer.length < 5) {
        // maximum of 3 number can be inputted
        userAnswer += button;
      }
    });
  }

  // check if user is correct or not
  void checkResult() {
    int ans = numberA - numberB;
    if (ans == double.parse(userAnswer)) {
      score = score + 1;
      goToNextQuestion();
      // showDialog(
      //     context: context,
      //     builder: (context) {
      //       return ResultMessage(
      //         message: 'Correct!',
      //         onTap: goToNextQuestion,
      //         icon: Icons.arrow_forward,
      //       );
      //     });
    } else {
      goBackToQuestion();
      // showDialog(
      //     context: context,
      //     builder: (context) {
      //       return ResultMessage(
      //         message: 'Sorry try again',
      //         onTap: goBackToQuestion,
      //         icon: Icons.rotate_left,
      //       );
      //     });
    }
  }

  int _start = 30;

  void _startCountDown() {
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (_start > 0) {
        setState(() {
          _start--;
        });
      } else {
        timer.cancel();
        showDialog(
            context: context,
            builder: (context) {
              return ResultMessage(
                message: 'Time Out !!!',
                message1: 'Play Again',
                onTap: playAgain,
                icon: Icons.rotate_left,
              );
            });
      }
    });
  }

  void playAgain() {
    score = 0;
    goToNextQuestion();
    Navigator.of(context).pop();
    _start = 30;
    _startCountDown();
  }

  // go to next question
  void goToNextQuestion() {
    // dismiss alert dialog
    // Navigator.of(context).pop();

    // reset values
    setState(() {
      userAnswer = '';
    });

    // create a new question
    numberA = randomNumber.nextInt(10);
    numberB = randomNumber.nextInt(10);
  }

  // go back to question
  void goBackToQuestion() {
    // dismiss alert dialog
    // Navigator.of(context).pop();

    setState(() {
      userAnswer = '';
    });
  }

  @override
  void initState() {
    super.initState();
    _startCountDown();
  }

  @override
  Widget build(BuildContext context) {
    final bool isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    final double headerHeight = isPortrait ? 90 : 60;
    final double inputSize = isPortrait ? 160 : 60;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown[700],
        elevation: 0.0,
        title: Text(
          "Subtraction",
          style: TextStyle(fontSize: 24),
        ),
      ),
      backgroundColor: Colors.brown,
      body: isPortrait
          ? _buildPortraitLayout(headerHeight, inputSize)
          : _buildLandscapeLayout(headerHeight, inputSize),
    );
  }

  Widget _buildPortraitLayout(double headerHeight, double inputSize) {
    return Column(
      children: [
        ScoreTime(headerHeight, inputSize),
        // question
        Expanded(
          child: Container(
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // qeustion
                  Text(
                    numberA.toString() + ' - ' + numberB.toString() + ' = ',
                    style: whiteTextStyle,
                  ),

                  // answer box
                  Container(
                    height: 50,
                    width: 110,
                    decoration: BoxDecoration(
                      color: Colors.brown[700],
                      borderRadius: BorderRadius.circular(4),
                    ),
                    child: Center(
                      child: Text(
                        userAnswer,
                        style: whiteTextStyle,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),

        // numpad
        Expanded(
          flex: 2,
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              final double itemWidth = constraints.maxWidth / 4.3;
              final double itemHeight = constraints.maxHeight / 4.5;

              return GridView.builder(
                itemCount: NumpberpadProvider().countNumpad,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                  childAspectRatio: itemWidth / itemHeight,
                ),
                itemBuilder: (context, index) {
                  return MyButtonSub(
                    child: NumpberpadProvider().numberPad[index],
                    onTap: () => buttonTapped(NumpberpadProvider().numberPad[index]),
                  );
                },
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildLandscapeLayout(double headerHeight, double inputSize) {
    return Column(
      children: [
        ScoreTime(headerHeight, inputSize),
        Expanded(
          child: Row(
            children: [
              // question
              Expanded(
                flex: 1,
                child: Container(
                  height: double.infinity,
                  padding: EdgeInsets.only(bottom: 70),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // qeustion
                        Text(
                          numberA.toString() +
                              ' - ' +
                              numberB.toString() +
                              ' = ',
                          style: whiteTextStyle,
                        ),

                        // answer box
                        Container(
                          height: 50,
                          width: 110,
                          decoration: BoxDecoration(
                            color: Colors.brown[700],
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: Center(
                            child: Text(
                              userAnswer,
                              style: whiteTextStyle,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              // numpad
              Expanded(
                  flex: 1,
                  child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: LayoutBuilder(
                        builder:
                            (BuildContext context, BoxConstraints constraints) {
                          final double itemWidth = constraints.maxWidth / 4.3;
                          final double itemHeight = constraints.maxHeight / 4.5;

                          return GridView.builder(
                            itemCount: NumpberpadProvider().countNumpad,
                            physics: const NeverScrollableScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              childAspectRatio: itemWidth / itemHeight,
                            ),
                            itemBuilder: (context, index) {
                              return MyButtonSub(
                                child: NumpberpadProvider().numberPad[index],
                                onTap: () => buttonTapped(NumpberpadProvider().numberPad[index]),
                              );
                            },
                          );
                        },
                      ))),
            ],
          ),
        ),
      ],
    );
  }

  // _showDialog() {
  //   showDialog(
  //     context: context,
  //     builder: (context) => AlertDialog(
  //         title: Text("Time's up!"),
  //         content: Text("Play again"),
  //         actions: [
  //           GestureDetector(
  //             onTap: () {
  //               Navigator.push(
  //                 context,
  //                 MaterialPageRoute(builder: (context) => Menu()),
  //               );
  //             },
  //             child: Text(
  //               "No",
  //               style: TextStyle(
  //                 fontSize: 16,
  //                 color: Colors.brown,
  //               ),
  //             ),
  //           ),
  //           Padding(padding: EdgeInsets.all(10)),
  //           GestureDetector(
  //             onTap: () {
  //               playAgain();
  //             },
  //             child: Text(
  //               "Yes",
  //               style: TextStyle(
  //                 fontSize: 16,
  //                 color: Colors.green,
  //               ),
  //             ),
  //           ),
  //         ]),
  //   );
  // }
  Widget ScoreTime(double headerHeight, double inputSize) {
    return Container(
      height: headerHeight,
      width: MediaQuery.of(context).size.width,
      color: Colors.brown[600],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            "Score: " + score.toString(),
            style: TextStyle(fontSize: 24, color: Colors.white),
          ),
          SizedBox(height: 8),
          Text(
            "Time: "
            '$_start',
            style: TextStyle(fontSize: 24, color: Colors.white),
          ),
        ],
      ),
    );
  }
}
