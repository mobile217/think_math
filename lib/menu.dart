import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'add.dart';
import 'div.dart';
import 'models/countdownProvider.dart';
import 'mul.dart';
import 'sub.dart';

class Menu extends StatefulWidget {
  const Menu({super.key});

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          "Math Game",
          style: TextStyle(color: Colors.black, fontSize: 24),
        ),
      ),
      body: OrientationBuilder(builder: (context, orientation) {
        if (orientation == Orientation.portrait) {
          return _buildPortraitLayout(context);
        } else {
          return _buildLandscapeLayout(context);
        }
      }),
    );
  }

  Widget _buildPortraitLayout(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Padding(
      padding: EdgeInsets.all(20),
      child: Container(
        child: GridView.count(
            crossAxisCount: 2,
            children: [
              InkWell(
                onTap: () {
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Add(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.cyan,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "+",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.3),
                        ),
                      ],
                    )),
              ),
              InkWell(
                onTap: () {
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Sub(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.brown,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "–",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.3),
                        ),
                      ],
                    )),
              ),
              InkWell(
                onTap: () {
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Mul(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.pink,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "×",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.3),
                        ),
                      ],
                    )),
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => Div(onProcess: _startCountDown)),
                  // );
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Div(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.orange,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "÷",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.3),
                        ),
                      ],
                    )),
              ),
            ],
            mainAxisSpacing: 10,
            crossAxisSpacing: 10),
      ),
    );
  }

  Widget _buildLandscapeLayout(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Padding(
      padding: EdgeInsets.all(20),
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        final double itemWidth = constraints.maxWidth / 4;
        final double itemHeight = constraints.maxHeight / 4.2;
        return GridView.count(
            crossAxisCount: 2,
            childAspectRatio: itemWidth / itemHeight,
            children: [
              InkWell(
                onTap: () {
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Add(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.cyan,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "+",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.1),
                        ),
                      ],
                    )),
              ),
              InkWell(
                onTap: () {
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Sub(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.brown,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "–",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.1),
                        ),
                      ],
                    )),
              ),
              InkWell(
                onTap: () {
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Mul(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.pink,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "×",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.1),
                        ),
                      ],
                    )),
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => Div(onProcess: _startCountDown)),
                  // );
                  context.read<StartCountdownModel>().startCountDown();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Div(),
                    ),
                  );
                },
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.orange,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "÷",
                          style: TextStyle(
                              color: Colors.white, fontSize: screenWidth * 0.1),
                        ),
                      ],
                    )),
              ),
            ],
            mainAxisSpacing: 10,
            crossAxisSpacing: 10);
      }),
    );
  }

  // Widget _buildLandscapeLayout(BuildContext context) {
  //   double screenWidth = MediaQuery.of(context).size.width;
  //   double screenHeight = MediaQuery.of(context).size.height;

  //   return Padding(
  //     padding: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
  //     child: Row(
  //       crossAxisAlignment: CrossAxisAlignment.stretch,
  //       children: [
  //         Expanded(
  //           child: InkWell(
  //             onTap: () {
  //               Navigator.push(
  //                 context,
  //                 MaterialPageRoute(
  //                   builder: (context) => Add(onProcess: _startCountDown),
  //                 ),
  //               );
  //             },
  //             child: Container(
  //               decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.circular(10),
  //                 color: Colors.cyan,
  //               ),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   Text(
  //                     "+",
  //                     style: TextStyle(
  //                         color: Colors.white, fontSize: screenHeight * 0.1),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ),
  //         ),
  //         SizedBox(height: 10),
  //         Expanded(
  //           child: InkWell(
  //             onTap: () {
  //               Navigator.push(
  //                 context,
  //                 MaterialPageRoute(
  //                   builder: (context) => Sub(onProcess: _startCountDown),
  //                 ),
  //               );
  //             },
  //             child: Container(
  //               decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.circular(10),
  //                 color: Colors.brown,
  //               ),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   Text(
  //                     "–",
  //                     style: TextStyle(
  //                         color: Colors.white, fontSize: screenHeight * 0.1),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ),
  //         ),
  //         SizedBox(height: 10),
  //         Expanded(
  //           child: InkWell(
  //             onTap: () {
  //               Navigator.push(
  //                 context,
  //                 MaterialPageRoute(
  //                   builder: (context) => Mul(onProcess: _startCountDown),
  //                 ),
  //               );
  //             },
  //             child: Container(
  //               decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.circular(10),
  //                 color: Colors.pink,
  //               ),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   Text(
  //                     "×",
  //                     style: TextStyle(
  //                         color: Colors.white, fontSize: screenHeight * 0.1),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ),
  //         ),
  //         SizedBox(height: 10),
  //         Expanded(
  //           child: InkWell(
  //             onTap: () {
  //               Navigator.push(
  //                 context,
  //                 MaterialPageRoute(
  //                   builder: (context) => Div(onProcess: _startCountDown),
  //                 ),
  //               );
  //             },
  //             child: Container(
  //               decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.circular(10),
  //                 color: Colors.orange,
  //               ),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   Text(
  //                     "÷",
  //                     style: TextStyle(
  //                         color: Colors.white, fontSize: screenHeight * 0.1),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
