import 'package:flutter/widgets.dart';

class NumpberpadProvider with ChangeNotifier {
  List<String> numberPad = [
    '7',
    '8',
    '9',
    'C',
    '4',
    '5',
    '6',
    'DEL',
    '1',
    '2',
    '3',
    '=',
    '0',
    '.',
    '-'
  ];

  int get countNumpad => numberPad.length;
  List<String> get getNumpad => numberPad;
}
