import 'dart:async';

import 'package:flutter/widgets.dart';

class StartCountdownModel with ChangeNotifier {
   int _start = 30;

  void startCountDown() {
    Timer.periodic(Duration(seconds: 1), (timer) {
      if (_start > 0) {
        _start--;
        notifyListeners();
      } else {
        timer.cancel();
      }
    });
  }

  //เพิ่มเมธอด get สำหรับให้ widget อื่น ๆ เข้าถึง _start
  int get start => _start;
}